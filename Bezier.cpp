#include "Bezier.h"
#include "Line.h"
#include <vector>
#include <iostream>
using namespace std;

//function flattenCurve(curve, segmentCount) :
//	step = 1 / segmentCount;
//coordinates = [curve.getXValue(0), curve.getYValue(0)]
//for (i = 1; i <= segmentCount; i++) :
//	t = i * step;
//coordinates.push[curve.getXValue(t), curve.getYValue(t)]
//return coordinates;

pair<float,float> Benzier2(float t, Vector2D p1, Vector2D p2, Vector2D p3)
{
	float t2 = t * t;
	float mt = 1 - t;
	float mt2 = mt * mt;
	return { p1.x*mt2 + p2.x * 2 * mt*t + p3.x*t2 , p1.y*mt2 + p2.y * 2 * mt*t + p3.y*t2 };
}

//function Bezier(3, t, w[]) :
//	t2 = t * t
//	t3 = t2 * t
//	mt = 1 - t
//	mt2 = mt * mt
//	mt3 = mt2 * mt
//	return w[0] * mt3 + 3 * w[1] * mt2*t + 3 * w[2] * mt*t2 + w[3] * t3

pair<float, float> Benzier3(float t, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	float t2 = t * t;
	float t3 = t2 * t;
	float mt = 1 - t;
	float mt2 = mt * mt;
	float mt3 = mt2 * mt;
	return { p1.x*mt3+3*p2.x*mt2*t+3*p3.x*mt*t2+p4.x*t3, p1.y*mt3 + 3 * p2.y*mt2*t + 3 * p3.y*mt*t2 + p4.y*t3 };
}

vector<pair<float, float>> flattenCurve2(float segmentCount, Vector2D p1, Vector2D p2, Vector2D p3)
{
	float step = 1 / segmentCount;
	vector<pair<float, float>> coordinate;
	coordinate.push_back({ Benzier2(0,p1,p2,p3).first,Benzier2(0,p1,p2,p3).second });
	for (int i = 1; i < segmentCount; i++)
	{
		float t = i * step;
		auto temp = Benzier2(t, p1, p2, p3);
		coordinate.push_back({ temp.first,temp.second });
	}
	return coordinate;
}

vector<pair<float, float>> flattenCurve3(float segmentCount, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	float step = 1 / segmentCount;
	vector<pair<float, float>> coordinate;
	coordinate.push_back({ Benzier3(0,p1,p2,p3,p4).first,Benzier3(0,p1,p2,p3,p4).second });
	for (int i = 1; i < segmentCount; i++)
	{
		float t = i * step;
		auto temp = Benzier3(t, p1, p2, p3, p4);
		coordinate.push_back({ temp.first,temp.second });
	}
	return coordinate;
}

//function drawFlattenedCurve(curve, segmentCount) :
//	coordinates = flattenCurve(curve, segmentCount)
//	coord = coordinates[0], _coords;
//for (i = 1; i < coordinates.length; i++) :
//	_coords = coordinates[i]
//	line(coords, _coords)
//	coords = _coords

void drawFlattenedCurve2(SDL_Renderer *ren, int segmentCount, Vector2D p1, Vector2D p2, Vector2D p3)
{
	auto coordinates = flattenCurve2(segmentCount, p1, p2, p3);
	auto coord = coordinates[0];
	pair<float, float> _coords;
	SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
	for (int i = 1; i < coordinates.size(); i++)
	{
		_coords = coordinates[i];
		Bresenham_Line(coord.first + 0.5, coord.second + 0.5, _coords.first + 0.5, _coords.second + 0.5, ren);
		coord = _coords;
	}
}

void drawFlattenedCurve3(SDL_Renderer *ren, int segmentCount, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	auto coordinates = flattenCurve3(segmentCount, p1, p2, p3, p4);
	auto coord = coordinates[0];
	pair<float, float> _coords;
	SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
	for (int i = 1; i < coordinates.size(); i++)
	{
		_coords = coordinates[i];
		Bresenham_Line(coord.first + 0.5, coord.second + 0.5, _coords.first + 0.5, _coords.second + 0.5, ren);
		coord = _coords;
	}
}

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	drawFlattenedCurve2(ren, 200, p1, p2, p3);
}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	drawFlattenedCurve3(ren, 200, p1, p2, p3, p4);
}


