#include <iostream>
#include <SDL.h>
#include "Line.h"
#include "Bezier.h"

using namespace std;

const int WIDTH = 800;
const int HEIGHT = 1000;

SDL_Event event;

bool Inside(SDL_Rect* rect, int x, int y)
{
	//Check if mouse is in button
	bool inside = true;

	//Mouse is left of the button
	if (x < rect->x)
	{
		inside = false;
	}
	//Mouse is right of the button
	else if (x > rect->x + rect->w)
	{
		inside = false;
	}
	//Mouse above the button
	else if (y < rect->y)
	{
		inside = false;
	}
	//Mouse below the button
	else if (y > rect->y + rect->h)
	{
		inside = false;
	}
	return inside;
}


int main(int, char**) {
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL) {
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

	//YOU CAN INSERT CODE FOR TESTING HERE

	Vector2D p1(22, 215), p2(94, 43), p3(140, 258), p4(213, 150);


	SDL_Rect *rect1 = new SDL_Rect();
	rect1->x = p1.x - 3;
	rect1->y = p1.y - 3;
	rect1->w = 6;
	rect1->h = 6;

	SDL_Rect *rect2 = new SDL_Rect();
	rect2->x = p2.x - 3;
	rect2->y = p2.y - 3;
	rect2->w = 6;
	rect2->h = 6;

	SDL_Rect *rect3 = new SDL_Rect();
	rect3->x = p3.x - 3;
	rect3->y = p3.y - 3;
	rect3->w = 6;
	rect3->h = 6;

	SDL_Rect *rect4 = new SDL_Rect();
	rect4->x = p4.x - 3;
	rect4->y = p4.y - 3;
	rect4->w = 6;
	rect4->h = 6;

	SDL_Color colorCurve = { 100, 20, 40, 255 }, colorRect = { 0, 255, 40, 255 };
	SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);
	SDL_RenderDrawRect(ren, rect1);
	SDL_RenderDrawRect(ren, rect2);
	SDL_RenderDrawRect(ren, rect3);
	SDL_RenderDrawRect(ren, rect4);
	SDL_SetRenderDrawColor(ren, 255, 255, 0, 255);
	Bresenham_Line(p1.x, p1.y, p2.x, p2.y, ren);
	Bresenham_Line(p2.x, p2.y, p3.x, p3.y, ren);
	Bresenham_Line(p3.x, p3.y, p4.x, p4.y, ren);
	SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
	//DrawCurve2(ren, p1, p2, p3);
	DrawCurve3(ren, p1, p2, p3, p4);
	SDL_RenderPresent(ren);
	//Take a quick break after all that hard work
	//Quit if happen QUIT event
	bool running = true;
	int x, y;
	int signal[] = { 0, 0 ,0 ,0 };
	Vector2D temp;
	while (running)
	{
		//If there's events to handle
		if (SDL_PollEvent(&event))
		{
			//HANDLE MOUSE EVENTS!!!
			if (event.type == SDL_MOUSEBUTTONDOWN)
			{
				SDL_GetMouseState(&x, &y);
				if (Inside(rect1, x, y))
					signal[0] = 1;
				if (Inside(rect2, x, y))
					signal[1] = 1;
				if (Inside(rect3, x, y))
					signal[2] = 1;
				if (Inside(rect4, x, y))
					signal[3] = 1;
			}
			if (event.type == SDL_MOUSEBUTTONUP)
			{
				SDL_GetMouseState(&x, &y);
				if (signal[0] == 1)
				{
					p1.set(x, y);
					rect1->x = p1.x;
					rect1->y = p1.y;
					signal[0] = 0;
				}
				if (signal[1] == 1)
				{
					p2.set(x, y);
					rect2->x = p2.x;
					rect2->y = p2.y;
					signal[1] = 0;
				}
				if (signal[2] == 1)
				{
					p3.set(x, y);
					rect3->x = p3.x;
					rect3->y = p3.y;
					signal[2] = 0;
				}
				if (signal[3] == 1)
				{
					p4.set(x, y);
					rect4->x = p4.x;
					rect4->y = p4.y;
					signal[3] = 0;
				}
					SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
					SDL_RenderClear(ren);
					SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);
					SDL_RenderDrawRect(ren, rect1);
					SDL_RenderDrawRect(ren, rect2);
					SDL_RenderDrawRect(ren, rect3);
					SDL_RenderDrawRect(ren, rect4);
					SDL_SetRenderDrawColor(ren, 255, 255, 0, 255);
					Bresenham_Line(p1.x, p1.y, p2.x, p2.y, ren);
					Bresenham_Line(p2.x, p2.y, p3.x, p3.y, ren);
					Bresenham_Line(p3.x, p3.y, p4.x, p4.y, ren);
					SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
					//DrawCurve2(ren, p1, p2, p3);
					DrawCurve3(ren, p1, p2, p3, p4);
					SDL_RenderPresent(ren);
			}
			SDL_RenderPresent(ren);
			//If the user has Xed out the window
			if (event.type == SDL_QUIT)
			{
				//Quit the program
				running = false;
			}
		}

	}

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
